寶物殿的面前，飄蕩著緊張兮兮的氛圍。

寶物殿『白狼之巣』敞開的入口處。
面對曾經『幻影』看守的地方，獵人們以隊伍為一個單位，各自做好最終的確認。

對策已經決定了。不對，是原本能使用的對策就很少。

在『白狼之巣』前面的一塊空地，『黑金十字』占據以那為中心的區域。

隊伍名的由來，以稱之為黑金的特殊合金製作的鎧甲，是利用現代最尖端技術所製造出來的。它不僅堅固，還能以柔軟來減緩承受魔法時產生的衝擊，是用上據說最接近寶具的素材所造。
在那鎧甲守護下，久經鍛煉的身體正顫抖著。這不是害怕，而是亢奮。

斯維恩・安格是六級獵人。他是具有別稱的實力者，在尋寶獵人當中也是屬於一流的──儘管不會神機妙算，也沒有預知未來的能力。

即便如此，他也是一名獵人。
過去，他們隊伍齊心協力，跨過諸多的修羅場。面對這項風險更甚以往的委託，如今斯維恩是既感到害怕，又覺得興奮。

本次參戰的隊伍共計十二支。
隊伍的平均人數為六人。總人數不到一百。以軍隊而言數量還為之過少，可每一個人都是經過寶物殿鍛鍊出來的獵人。

有這麼多受瑪娜源強化的獵人聚集在一座寶物殿，是鮮少會發生的事情。他們會具備其數量以上的實力，還有不少人擁有武器型的寶具。

然而，縱使是這樣，他們也沒有一絲鬆懈的樣子。這是因為『足跡』所屬的獵人都理解到『千之試練』的危險性。而那些外來的獵人也全都被『足跡』成員的氣魄所鎮住。

『白狼之巣』是洞窟型的寶物殿。寶物殿往往都有這樣的一面（不如說，正因為是這樣才會有尋寶獵人），無法利用人數上的優勢大。

斯維恩決定分散部隊壓制內部。由於人數過多，至少一定要避免撤退時行動出現妨礙。

劃分好各自要調查的區域，以隊伍為一個單位進行探查。
聯絡以笛聲來決定。根據連續吹響的次數來改變信號的意思。

遇到緊急情況或是發現什麼時都要先撤退。要是找到那只魔物就引誘到外頭，所有人一起上。
即便平安無事也要在一段時間內回來。如果不回來，那就當做是無暇吹笛就被殺死了。

不進去寶物殿的隊伍則是在外頭待命。儘管有逐隊被擊殺的風險，不過應該可以避免一次性全滅的結局吧。

沒有任何敵人的情報實在是棘手。不對，光知道有敵人的存在就是幸運了吧。
獵人不會怠慢事先的調查。要是遭遇到這樣的狀況是很糟糕的。

還不如挑戰未知的區域比較輕鬆。

斯維恩又咂咂嘴，以銳利的眼神瞪視著寶物殿。

「試練、試練嗎⋯⋯該死的克萊伊，給我按上一件麻煩事。之後我一定要揍你一頓」
「嘴上是這麼說，明明怕『絕影』就怕得要死」
「吵死了。單憑弓箭不可能射中她吧！相性實在太差了」

對於開著玩笑的隊伍成員，斯維恩是氣沖沖地怒吼回去。

絲特莉特制的史萊姆殲滅劑就在塔莉亞身上，而塔莉亞則是跟她的伙伴則在寶物殿門口的不遠處待命中。也許是正處於緊張狀態，只見她拚命地讓呼吸平穩下來。

塔莉亞是──不對，她所擁有的黑色藥水才是這一次的王牌。
當然，他不會完全依賴在這個東西上。如果可以通過其他獵人的攻擊──遠距離的魔法或弓箭擊殺它，那是再好不過了。

不過，能夠提前找到對策是一種僥倖，以備這些攻擊都無效的情況下還有方法可用。

錬金術師的戰鬥能力比較匱乏。反過來說，認真做好準備的時候，其應對能力遠勝於任何職業。
塔莉亞的技術還未成熟，不過她擁有的藥水是曾經被稱之為『最優秀』的錬金術師所制。效果是毋庸置疑。

這時，有人從背後搭話。
那是略帶顫抖的聲音。

「⋯⋯那個⋯⋯絲特莉是哪一位？我看大家都挺相信她的樣子⋯⋯」
「啊啊，赫里克，你還沒見過她吧⋯⋯」

赫里克加入『黑金十字』還是在半年前的時候。當時『嘆息之亡靈』已經是位居于頂尖的隊伍。

『嘆息之亡靈』的成員全都具備獵人以外的一面。
其中作為錬金術師進行工作的絲特莉是極其忙碌。隨著她總不見踪影，名字自然而然就不怎麼聽得到了。

「畢竟絲特莉最近都不怎麼露面呢」

成員之一，魔導師瑪麗愛特像是感到懷念似的眯細眼睛。
然而，在那眼神深處有著強烈的恐懼。

具備優異能力的人，有時不但令人憧憬，也令人恐懼。
斯維恩也經常被人投以夾帶畏懼的眼神，瑪麗愛特與其他成員應該也都受過別人的嫉妒吧。

絲特莉・斯瑪特也是如此。

她腦袋轉得很快，吸收一切的知識，甚至在這座帝都，有幾名兼有最高學府的優異術師當中，她也是出類拔萃，自身作為錬金術師的天賦異稟是人人都羨慕的。被評價為最接近錬金術師的夙願之一『賢者之石』的術師。

然而，令人恐懼的並不是她的能力。

斯維恩望著樣子隱約有所顧慮的赫里克。他那柔和的眼神與那位『錬金術師』有些相似。

斯維恩暫時停止呼吸，以額頭都要皺出眉頭的樣子說道。

「嘛，一句話來說⋯⋯絲特莉⋯⋯她是『強大』的弱者」
「強大的⋯⋯弱者？」

她很強，很有才能。

可更主要的，是那人本身的『異樣』誰都無法理解。

因此，當榮光成為往事時，誰都不會提到她的名字。
並不是忌諱她。只不過無論是誰都會自然而然地不再提到這名字。仿彿就像是從記憶當中消失不見一樣。

而事實上，如今『足跡』也開始出現像赫里克這樣不知道她名字的成員。

斯維恩抬起頭，望向手握裝著黑色藥水的瓶子的塔莉亞。

「而且包括我們『黑金十字』在內，有幾支隊伍都是因為絲特莉的勸說才參加『起始之足跡』的創建。曾經在『嘆息之亡靈』中──她可是僅次於克萊伊的高級認定獵人，是極為出色的『錬金術師』」
「斯維恩，我們這邊準備好了喔」
「啊啊，我知道了。抱歉，接下來的事情以後再說」

萊路呼喚以後，斯維恩往前踏出一步。

戰意充分，沒有人感到膽怯。

『足跡』很優秀。平均等級這麼高是有理由的。

弱者已經遭到淘汰。膽小鬼早就在以前脫離氏族。
在這裡的都是多少跨越過試練的人員，正所謂是精英。與此同時，他們也是戰友。

這件事會使人產生自信。

『起始之足跡』很強。

由頂尖的隊伍所率領。
設備齊全，成形的管理體制。
不過要讓斯維恩來說，這些東西都只不過是附帶罷了。

正因為共同經歷過屍山血海的戰場，故而團結一致，這才是這個氏族的精髓。正因為如此，這氏族可以在幾年內發展到現在這麼大。
足跡的象徵意義就是刻下至今走來的軌跡。那會在不知不覺成為一種驕傲。

作為獵人賭上性命的理由是十分充分的。

儘管這裡面也有外來的隊伍，不過誰會管得了他們那麼多。

斯維恩大口吸氣，用足以震動草木的聲音吼道。
戰意逐漸高潮。高揚感仿彿傳達出去了一般，獵人們都緊繃著表情。

「喂，你們這幫傢伙，給我鼓起幹勁！蹂躪吧！刻下足跡！全員活著回去，向那個狗屎Master說這種程度沒什麼大不了的！」
「噢噢噢噢噢噢噢噢噢噢噢噢噢噢！」

爆發性的咆哮撼動圍繞著寶物殿的森林。
無論是足跡所屬的隊伍，還是外部參加的隊伍，全都吼到聲音要枯竭一般。

隨後，獵人們用難以稱之為一絲不亂的混亂勢頭，開始自己的侵略。

§

選好寶物殿，我拿上資料走在通往地下訓練場的樓梯，這時響起宛如切開風的銳利聲響。

氏族之家所打造的訓練設備與帝都各地存在的設備幾乎是沒有區別。由於被卷入訓練我就完了，所以我很少會到這邊露面。不過只要到地下，總會遇到某些人在確認身體狀況的場面。

雖然這頂多是訓練，可傳達過來的氣魄是貨真價實的。感覺到這些，我都陷入連自己也仿彿成為獵人的錯覺。

嘛，我姑且還是獵人就是了。

我有節奏地走下樓梯以後，推開位於地下一樓訓練場中，由金屬制成的厚重門扉（自訓練場建成以後遭到多次擊飛，後來又換好的）

地下一樓只能進行對練與確認身體動作。要使用魔法，還是到地下二樓牆壁有施加應對魔法處置的訓練場比較合適，而練習特殊技術則是要到擺置許多器具的地下三樓以後的樓層。

推開門的瞬間，冰冷的空氣與聲響迎接了我。

莉茲就站在空無一物的寬敞訓練場中心，由金屬制成的地面上。

與她對峙的則是緹諾。緹諾還是一如既往，身穿以黑色為基調的獵人裝扮，壓低身子瞪視著眼前比起自己還要矮的師傅。

（颯：我又看了幾遍，???莉茲你這麼矮的？）

推開門的聲音響徹於訓練場，不過漆黑的虹彩眯細起來，看樣子也沒注意到這邊。

從她那銳利的眼神，難以相信她平時總是遭到師傅的虐待。

另一方面，莉茲望向走近她的我，解除自己的架勢。

「啊，克萊伊。終於到我們出場了？」
「⋯」

莉茲以輕快的聲音詢問我，而緹諾則是面向她，黑髮隨之搖曳。隨著短暫的蓄力，釋放出由皮革手套所覆蓋的拳頭。
這一拳超越音速，響起一陣貫穿空氣的簡短聲響。我被她那樣子震撼住，不由得後退一步。

而莉茲則是沒有望向那邊，依然維持著笑容，側身躲開那神速的一擊。

「畢竟現在是在訓練中。要是把她打得遍體鱗傷，也許會在克萊伊拜託的事情中造成影響吧？」
「！！！」

緹諾的一擊都只能看見殘影了。化為水滴的汗水濺散而出，那黑髮後綁起的絲帶時不時地在視野中閃過。
重視速度的攻擊。她踏出一步的同時，低高度使出的踢技也襲向莉茲。

大氣正因那遠方都能傳達得來的強烈氣魄而有所搖動。再搭配她身上的衣物，那樣子簡直就像是一道黑風。

掃堂腿。直拳。掌擊。回旋踢。肘擊。這些一舉一動全都構成一種攻勢。

還差０.１秒就能準確命中。即便是如此激烈的驚險攻防，也宛如像是跳著武演一般，顯得十分美麗。
輕啟的雙唇吹出白色的氣息。轉身後同時釋放出的踢技掠過莉茲的眼前，擊落她幾根頭髮。雖然在我看來是擊中了，不過應該是以一紙之隔躲開了吧。

總覺得好厲害啊⋯⋯難怪莉茲會說她有才能。

即便忙著應付毫不停歇的攻勢，可莉茲也還是眯細雙眼，揚起嘴角笑著。

「吶？很厲害對吧？跟人家的極限不一樣吧。以前被指出沒有才華的時候，人家是很不開心的，不過看到小緹，果然還是覺得她有才能⋯⋯！！本來還以為不需要弟子的，幸好有聽從克萊伊的！」

莉茲剛來帝都成為『絕影』的弟子時，開口第一句話就指出她沒有近戰的才華。如今實力雖然增長到認定為六級的程度，可那時候的創傷還刻著她心中。

不是，在我看來完全搞不懂她們有什麼不同，在莉茲躲開有望弟子的攻擊時我就已經覺得她厲害了，到底還是她的目標太高了。

話說回來，以前看見的訓練中，緹諾的攻擊甚至都沒掠過莉茲的頭髮。實力差距看起來的確是縮小了。

現在是空手所以夠不著，不過只要使用她平時腰上掛著的短劍不就夠得著了？
即便如此，兩人的實力差距還是很明顯。當上弟子這幾年，緹諾說不定總是追在莉茲的後頭。

我心生佩服地想著這些事情，突然間緹諾發出像是硬擠出來的話。

「哈啊、哈啊⋯⋯完、全！打！不中──！為什麼！？」

明明一直在做著激烈的動作，可她那一擊更具威力了。她的拳頭只掠過莉茲的頭。
是莉茲稍微壓低了身子。

相較於發出悲痛聲的緹諾，莉茲則是完全相反，一直面帶著笑容。

以我的視力已經不知道發生什麼事。如果我是處於莉茲的立場，估計在第一擊就被擊暈過去吧。
莉茲拍起手，同時又以羽毛般的輕快步伐跳著舞步。

「你看看，小緹。還有一點！再稍微伸長一點？拼上性命！別放慢動作！集中精神！好，別後退！進攻吧！不要害怕！」
「⋯」

緹諾望著莉茲的眼裡，閃耀著熠熠生輝的光彩。就像是莉茲在戰鬥之前所展示的，仿彿像是耗損生命的眼神。

對於她們這幅樣子，我停下腳步，撓撓臉頰。

「⋯⋯難不成還游刃有餘？」
「咦？不然像是生命危機的樣子？」

不是，雖然我看不出是危機的樣子⋯⋯可你自己不都說極限不一樣了⋯⋯⋯

緹諾緊咬牙根，而莉茲則是瞪大眼。

也許是速度又提高了。我感覺響起的聲音更加激烈，更加沉重。
伸出的手腳都相當於一把槍，帶有同等的銳度。然而從剛才開始依然是沒觸碰到莉茲。
即便提高速度也沒擊中，就代表莉茲是以最低限度的動作躲開攻擊的吧。

緹諾的身高比起莉茲還要高。她的手腳也比較長，可還是夠不著。

「小緹，短劍」
「！」

在莉茲講完這句話的瞬間，緹諾瞪大眼睛。
她用手毫不猶豫地拔出腰邊的短劍。那不是用於訓練，刀刃有殘缺的武器，而是她平時帶到寶物殿，經過細致磨合的武器。

她維持那速度所橫掃的一擊──提升攻擊射程的那一擊，莉茲是面不改色地往後退避開了。即便眼前幾厘米處有白刃通過，莉茲也沒眨眼睛。
無論是拳頭，還是橫掃，就連讓注意力轉移到刀刃上的踢技，全都被莉茲以一紙之隔躲開。她笑著躲開這些甚至能看見殺意的攻擊，一副十分開心的樣子。

「訓練、小緹的同時，還能兼備人家、自己的、訓練！厲害吧？」
「⋯⋯嗯嗯，很厲害」

樣子還真是輕鬆啊⋯⋯看來緹諾要超越師傅還要花上不少的時間。

緹諾的呼吸急促。平時顯得潔白的肌膚也泛起紅色，瀏海因汗水而緊貼在額頭上。
即便如此，她依然沒有減緩攻勢。身體重心穩定，動作還是輕巧。

我如果是莉茲就給她打滿分了。假設我是緹諾，估計會對自己的實力感到滿意，再也不會接受訓練吧。

緹諾全力往前踏出，向上揮出一擊。而莉茲躲開後，輕巧地說道

「那就結束掉吧⋯⋯身體也熱身好了」

喂喂，剛剛是準備運動嗎⋯⋯⋯

無論攻擊被躲開幾次，即便是被完全看穿，緹諾那宛如風暴的連擊仍沒有減緩的跡象。
對於她毫無空隙的攻勢，莉茲是往前踏出一步。

應該是會擊中的。莉茲所處於的地方完全就在緹諾的攻擊範圍之內。
然而，踢技、拳擊、揮下的短劍，全都被莉茲猶如魔法一般所躲開。

我擦拭著眼睛，可還是不知道發生了什麼。只剩殘影的莉茲輕易地就壓制住緹諾伸出的手腕，還用腳掃過緹諾的腳下。
僅僅如此，這種令人覺得也許會永遠持續下去的場面。使得我產生這樣一種錯覺的緹諾就停下了動作。我不禁微微嘆了口氣。

緹諾姿勢奔潰，儘管她反射性地揮動四肢，然而手腕遭到壓制，身體又張開的狀態下，根本是無濟於事。
緹諾睜大眼睛，這時她身體已經倒在地上。細小的慘叫從她嘴裡叫出。
莉茲則是用手抓住躺在地上的緹諾的脖子處。

「好，結束」

倒在地上的緹諾身體激烈痙攣著。她那心臟的跳動聲都似乎傳到了這邊。
手腳不停顫抖，也許是脖子被抓得痛苦，緹諾眼含淚水，咳嗽起來。她的目光在空中徘徊，終於望見了我。

莉茲放開架在脖子上的手，拍手甩掉灰塵。

「哈啊、哈啊⋯⋯Ma、Ster？」
「接下來還有工作。小緹，你汗味好丑。別一直躺著那裡了，趕緊做好準備過來喔？　還想繼續讓克萊伊看到你出醜的樣子嗎？快點！」

對於師傅說的這些過分話，緹諾是瞪大眼睛。

不，其實休息一下再來也行──不如說我根本就沒有要拜託的事情，不去也無所謂⋯⋯現在緹諾需要的是讓身體歇息不是嗎。

我想著這些事情低頭望著她，而緹諾那原本因激烈運動而顯得紅彤彤的臉頰是更加泛紅，眼裡都啜著淚水。

「我、我現在就去！」
「⋯⋯誒？不，其實也──誒！？」

緹諾跳了起來。我忍不住後退。那感覺就像是屍體爬起來一樣。

她用直到剛才都還處於痙攣的腳，站穩身子。隨後搖搖晃晃地跑了出去。
雖然她現在看起來隨時會倒下，可還是以跟我全力一樣的速度離開。看樣子，我的速度跟不僅受傷、還極其疲憊的緹諾是相同的水準。

緹諾用身體直接撞開門扉，消失到訓練場外面。
真擔心她會不會在中途倒下。

「什麼時候開始訓練的？」
「誒？一直？」

完全是牛頭不對馬嘴。
一直是指⋯⋯目送萊路他們前往『白狼之巣』後，已經過了大約兩小時的時間耶⋯⋯⋯

很明顯是做過頭了。明明獵人的休息也是分內工作。

另一方面，莉茲這邊一滴汗水都沒流。你真的是人類嗎？

「還是不要帶緹諾過去吧？」
「誒～，沒問題的啦？小緹也能成為一定的戰力⋯⋯克萊伊就是這方面太苛刻了呢。⋯⋯你拿著的是什麼？」

到底是哪裡苛刻了啊，我又不是當她是戰力之外。
受到莉茲的詆毀，我皺著眉頭。隨後她詢問我，我嘆了口氣，將資料室找來的寶物殿資料夾遞給她。

氏族有收集過關於帝都周邊寶物殿的大部分情報。這是我久違地呆在資料室找出來的資料。
說是找出來的，其實我只是尋找原先就整理好的資料而已。

莉茲翻過頁面，可愛地歪著頭。

「嗯嗯～？都是等級較低的地方？而且好像還有幾處來著？」

寶物殿是藉由瑪娜源的積存而形成的。基於這性質，比起高等級的寶物殿，還是低等級的寶物殿會比較多。
在地脈有幾條穿過的帝都中，低等級的寶物殿就存在十多個。

即便篩選出遠離『白狼之巣』的條件，優柔寡斷的我還是沒辦法選定出其中一個。
為了掩飾這件事實，我露出笑容回答道。

「是啊，我想讓緹諾自己選擇喜歡的地方啦」

雖說平時她總是那麼慘，可只要這樣做就一定沒問題了。