　驕傲自大的猴子們，向天空舉起雙臂。

　在空中，出現了如同橄欖球大小的茶色石彈。


　嘛，就這樣看著吧

　首先在這一時刻，對著對象，也就是向著敵人的魔法，架起右臂。

　正確地說，雖然沒有必要把手舉起來，但是這樣做能更容易集中意識，感覺技能的精度稍微提高了點。

　然後，對著敵人的魔法，在心中，輕微地灌注意念。


　――比起那些猴子，能聽從我的請求嗎？然後。


　恐怕，即使我以適當的形式來命令也應該沒問題，但是，我可是有身為最低限度且不能讓步的文明人的自豪。所以，我帶著禮儀從心底裏向對方請求。

　那麼，最後是，我的口令。

　這個口令，是發動技能的扳機。

　我想這恐怕相當於是魔法“詠唱”的方式吧。

　口令這一言語，其實用什麼都行。

　比如說，以前有人說過的〈芝麻開門〉也行，也可以是〈ちちんぷいぷい〉。（查了很多也沒有具體意思，反正就是一種咒語，音譯過來就是痴痴噗噗...）

　這次要說什麼好呢……。

「〈受死吧，monkey們〉!!!」

　原本是茶色的石彈群，眼看著逐漸染上黑色。

　待變色完後，就說明石彈已經被支配了。敵人的魔法控制權，已經轉移到我的手上。

　……但是，一如既往的爛口令可不好。

　不管怎麼說，這可是我唯一的必殺技啊。在近期內，得想個帥氣的必殺名。

「呼哈哈哈！野蠻的monkey們！讓你們知曉真正的靈長類最強的力量吧！」

漆黑的“橄欖球”群biubiu地發射出去，追趕著猴子們。（biubiu就是擬聲詞啦，能意會就行）

　猴子們恐怖地絕叫，被嚇得四處逃散，最後消失在懸崖的對面。

　活該啊。

　啊，有一隻從懸崖上掉下來了。

被大量的猴群包圍，從差點受到密集圍攻的一戰中，已經過去了半天時間。

　結果，跟猴子群打了幾次交道後，我已經完全掌握了這個技能。

　這好厲害啊。一旦奪取了敵人的魔法控制權，然後就能隨心所欲地使用了。

　敵人的魔法，就像是操作自己的手腳一樣。

　以前，使岩石做的門變成黑色，然後將其撬開來，使拘束石頭做的書的鎖爆炸，恐怕罪魁禍首就是這招吧。

　只是，這一招如果不等待對方使用魔法的話就發動不了，使得戰鬥完全變成專守防禦的局面，這可是個難點，不過。

　再加上，在不用魔法直接攻打過來的武鬥派猴子面前，就顯得很無力。

　我們的隊伍裡依舊除了哥雷太郎的拳擊和踢技的通常攻擊手段就沒有其他了。

話說回來，奪走了魔法的控制權的那一刻，魔獸就好像完全喪失了戰意。

　看到了自己放出的石彈開始變色時，猴子的表情變化得很厲害。這是何等悲壯的神情啊。

　就像是一下子喪失了自己最信賴的東西一樣，絕望的表情。

　該如何表達那樣的情緒呢。嗯姆~~。

　對了。那種表情簡直像是，常年陪伴的妻子被來維修管道的工人給睡了一樣――

「……好了，就這樣決定了」

　我向哥雷太郎的方向回頭看。

　然後，滿面笑容地說了出來。


「這個必殺技的名字就叫做，〈NTR〉吧！」（我就是因為這句才來追這一小說滴）


　怎樣，哥雷……嗚哇，臉靠太近了!?

　額，嗯。總覺得哥雷太郎好像在稱讚我的命名品味，似乎是有這樣的感覺。

　嘛，它是個好傢夥，假設我的命名品味是毀滅性的，就算存在那樣的平行世界，我覺得這傢伙也不會拋棄我的。

　就這樣，雖然這是我學會的第一個必殺技〈NTR〉，但實際上，使用這個技能和猴子的戰鬥中，也就是最初的那場戰鬥是最有用。

　在這之後的戰鬥，都是時不時發生。跟剛才我趕走的那一群，差不多也是20只左右。

　哥雷太郎似乎非常警戒著猴子的包圍，一發現猴子，就立即來回奔跑地除盡它們。

　無論是躲藏在岩石陰影處，還是在懸崖的死角處，都會馬上被發現並且被擊潰。

　好厲害。就像是，在廚房發現害蟲的主婦一樣毫不留情。

　說實話，哥雷太郎這傢伙，在我用出〈NTR〉的時候看上去好像很開心的樣子，所以偶爾會故意讓幾隻沒什麼危險的猴子穿過像是用來探索敵人的網。

　你真是個會照顧人的紳士啊，哥雷太郎喲……。


　但是，這傢伙明明有這麼厲害的感知能力，為什麼那個時候，卻讓那群猴子給包圍了呢？（一開始被包圍的時候）

　因為猴子單體的戰鬥力太弱了，所以就疏忽大意了吧。


　……不。倒不如說，哥雷太郎一開始就沒有把猴子作為敵人？


　突然想到那時候，我似乎在看著遠處的猴子，對著哥雷太郎，悠閑地解說關於地球上的猴子的事情。

　難道說，那種毫無危機的感覺就是原因嗎？也就是說全都是我的錯嗎？


　但是，如果是這樣的話，那是哥雷太郎是從哪裡開始把對方當做敵人的呢。

　我不覺得這個世界的魔物只有猴子，恐怕今後也會發生各種各樣的危險吧。預先掌握哥雷太郎的危機意識是如何判斷的，在考慮到今後的安全之上，意外地感覺還是挺重要的。

　最重要的是，我覺得挑起這場戰鬥的，該不會就是哥雷太郎吧。

　那個時候，到底是怎樣的狀況呢。

　陷入沉思的我，突然想起一件事。


　對了。我記得在一開始被攻擊之前，我好像是在稱讚猴子「真可愛」――


　突然就轟隆地響起一聲。

　哥雷太郎把岩石陰處的大猴子給踢飛了，猴子掉落到懸崖，一命嗚呼。


　額，這是在說什麼來著？

　對了，是在說大猴子的事。對此我有些在意的事。

　沿著主路向西行，感覺猴子的體型微妙地變大了。

　最初遇到的那群，是日本猴子的大小。

　但是現在個體的平均體型，都有種像是一開始那群猴子的boss的程度了。

　儘管如此，嘛，猴子跟哥雷太郎之間，有著螞蟻跟大象一樣的戰鬥力差別。

　道路上充滿了魔獸。

　而且，走了半天以上，事實是還沒有遇到過任何人。

　我隱約懷著不安，沿著日光傾斜的道路，向西前進。

---------

　夜空之下。我還沒有睡著。

　睡了一會，又醒了過來。

　「好、好冷......」

　刺骨般的寒冷，我一邊顫抖一邊嘟囔著。

　在月光下照明的吐息是雪白的。

　到了夜間氣溫會下降到這種地步嗎？這完全就是大陸性的氣候啊。

　那個盆地裡到底怎麼回事？

　這簡直就像是，只有那個地方是另一個世界一樣。

　到了這種程度的差別，我想到的線索只有琉貝烏·扎伊雷遺留下來的記述。

　我記得那傢伙好像寫了不僅僅關於盆地內的地形，也有寫與外界隔離的結界之類的。

　就是因為那個嗎？

　結界魔法是這麼厲害的嗎？我還以為只是相當於神社的結界一樣的程度.......。

　看守篝火的是哥雷太郎，所以火源沒有斷絕過。

　但是這種寒冷，僅靠篝火和一張薄毛毯是遠遠不夠的。

　話說回來，大約在日落之前，猴子的襲擊突然就停止了。

　它們是知道的。

　那些猴子們，現在在巢穴裡暖和地度過夜晚嗎？可惡……！

　在現代日本這個溫室長大的我，基本上是習慣不了自然的殘酷。

　真糟糕，這個。到目前為止，我都是以盆地內的氣候為前提來制定行動的計劃。

　到底該怎麼辦呢……。

　在走投無路的我面前，哥雷太郎坐在那。

　謹慎地將雙手張開。

　簡直就像是在邀請別人坐在自己的膝蓋上。

「喔，你這傢伙，能夠為我擋風嗎……？」

　抱歉，多虧有你，哥雷太郎。我，流下了可恥的眼淚。


　從前有偉人這樣說過

　――寒冷和飢餓會使內心變得軟弱。


　我坐在哥雷太郎的膝蓋上。

　好柔軟。像硅膠一樣。

　連疑問的餘地都沒有。這傢伙，可以自律地調整體表的硬度。

　這個也好，那個也好。全都是為了不傷害脆弱又矮小的我，非常用心地去照顧這樣的我。

　啊，又要哭出來了……。


「話說，哥雷喲。總覺得好溫暖啊，你的身體……？」

　不知為何，從哥雷太郎那感覺到了和善的溫暖。

　真的嗎。這到底是怎麼回事啊？

　已經不行了。對於無能無識愚昧無知的我，完全理解不了這原理……。

------


　然後大約30分鐘後。

　被溫暖柔軟的哥雷太郎的手臂和毛毯包裹著，我一邊啃著用篝火烤過的肉乾，一邊望著夜空。

　這個肉乾，好像沒有用鹽，而是用香料。

　稍微烤一下，味道就非常不錯。恐怕是高級品吧。


　頭頂上蔓延著一片如海一般深藍色的夜空。

　在夜空中懸掛的兩輪明月，十分美麗。

　果然，這個世界會有兩個月亮。重新想一想，不愧是是異世界啊。

　不，等一下。也許不止兩個。

　雖然是月亮，但歸根到底只是在這一行星上有衛星圍繞罷了。也許還會有三個或四個左右。等等，對哦。也就是說，要看情況而定……。


　微微地搖了搖頭。

　……不了，還是不要自顧自地像是很聰明一樣發表看法了。

　我對自己擁有的自然上的知識完全喪失了信心。說白了，就是風前殘燭的狀態了。

　啊，沒錯。那應該就是魔法之月吧。

　被仙人用氣功炮射中的話，說不定就會消失了。（這是在說龍珠的梗麼?）

回想起來，這是我第二次這樣眺望著這個世界深夜的天空。

第一次我在夜間這麼做的，其實就是被召喚來的第一天。

　這個世界沒有照明。不，不對。正確地說，恐怕那個隱蔽的房子也有跟岩石做的門的洞穴裡一樣的照明裝置，只是我不知道該怎麼使用。

　所以，一旦太陽落山了就趕緊睡覺。

　完全找不到跟火相關的器具，真是太辛酸了。恐怕原家主扎伊雷會用火屬性的魔法吧。


　順帶一提，我家的哥雷太郎非常擅長點火。

　沒錯。這可是土屬性魔法戰勝火屬性魔法的瞬間。（應該是說用土魔法做出的人偶能打火吧）

　我們這一搭檔，不，倒不如說這個世界已經不需要火屬性魔法了。這完全是過去的遺物了。僅限這一點，可以說是我創造出的哥雷太郎有著改變歷史的意義。

　哥雷太郎的力量和精巧如果要我說，那肯定是理所當然的，一轉眼就能點著火了。

　真是幫了大忙啊。對於哥雷太郎，我真的是無地自容啊。


　啪擦啪擦地，火焰迸發開來。

　抱著我的哥雷太郎，緩慢地在篝火上添上柴火。

　話說，盆地的外邊就如所見一樣，是一片荒野，但是枯木很多。所以，一點都不用擔心柴火的用量。

　有高樹大小的枯木林立著，讓人覺得會不會是樹林或森林的地方，好幾次經過了這樣的地方。

　這一帶，直到最近都是適合普通樹木生長的氣候嗎？

　那麼，現在為什麼會變成這樣呢？


　思考著事情的時候，迷迷糊糊地開始犯困了。

　這裡很暖和，肚子也填飽了，心情感覺很好。


　在哥雷太郎的手臂裡，無法形容的安心感，這到底是什麼呢？

……啊。就是那個。跟寒冬中裹著的羽絨被很相似。

　哥雷太郎喲，難道你就是我的被子嗎……!?

　悄悄接近的睡魔，在漸漸擦肩而過的意識中，我在朦朧中思考著。

　多虧了哥雷太郎，才會這麼暖和。可以安心地睡著了。

　不得不向這傢伙說聲謝謝才行。

　我基本上是個文明人，再加上，擁有作為日本人應有的美德。

　普通的日本文明人，在對溫暖的被褥傳達最大限度的感謝時，是會這樣述說的。


「我，要跟哥雷太郎結婚……」


　沒錯，向著被子求婚。

　一瞬間，感覺哥雷太郎抱著我的手臂有點使勁了。

　但是，那個時候，我的意識已經陷入了深深的假寐之中。