「哇、這樣的氣氛…」
露露忍不住的泄漏了想法，而全員都有同感。


到現在為止經過的迷宮，都發著能讓視野清楚看見的光亮。但第7層的土牆上，只有最低限度的照明而已。
而在黑暗中，不知道潛藏著甚麼。
「應該不會出現妖怪吧。我、對那個不是很在行啊」
語調非常低沉的瑪魯說著。
「難道說瑪魯，你在黑暗的地方也看的見？」
莉雅詢問著，點了點頭。
「獸人幾乎都是這樣喔」
原來如此，貓桑。
「土壁嗎。只要知道中心在哪，直接破壞走過去就好了…」
莉雅一如往常的說著，魔法使的兩人也放棄了。
這層的魔物主要是不死系，也會從牆壁爬出來。敵人前後來姑且不說，但是連旁邊都會出來，處理不了這是當然的。


探索變得極其困難。
而骷髏不只單純的骸骨。也有裝備豪華的骷髏，如果是以賣掉裝備為目的的話，非常歡迎。而像階層守護者程度的骸骨騎士普通的出現在這徘徊，相當嚴峻啊。
僵屍也是。噁心的屍體在動著，沒有甚麼戰鬥力。其中也有著怪力或是敏捷的特殊個體，但只要把頭打爆就不會動了。毒、和其他可以使用魔法治療之類的異常狀態。
這些不死系的，讓採集魔石的心情變得很不好。心情不好等問題，都是多虧了莉雅撲殺了附近一帶哥布林的福啊，負擔全都落在了其他成員身上了啊啊。


問題是，幽靈啊。


對著沒有形體的魔物攻擊有效，是因為昨天晚上每個人的武器都被賦予了魔法。
但是對於幽靈的攻擊，防御手段就很有限。
用魔法做出屏障，就只是一般的防御。但是在長時間的探索下，一直用魔法防御也不是辦法。
「這時候就要靠氣勢」
被幽靈攻擊還若無其事地莉雅是例外，就算是習慣疼痛的前衛戰士，幽靈的攻擊也吃不消啊。


幽靈的攻擊是，碰觸到直接奪取對方的體力和魔力。輕則頭暈重則站著暈過去，突然襲擊過來的恐怖感，就算是鎧甲也無法防御。


莉雅因為有著豐富的異常狀態耐性，強大的精神力和魔力的關係，在這邊絲毫不受影響。
幽靈在現實中是3D CG的，所以無法感受到。頂多雞皮疙瘩而已。


還有種族特性的東西，露露的攻擊對幽靈來說很有效。瑪魯靈活的運用五感，在接觸前就快速地避開了。


稍微休息片刻，一行人做著地圖。到現在為止的趨勢，守護者的房間是在中心沒錯。空白部分有很多，沒有餘裕填滿。
「一口氣穿過去。隨著時間流逝，我們消耗太多了」
莉雅不斷的爆破牆壁，一行人跳入了守護者的房間。


地下墓地讓人想到寒氣，有著高高的天花板。
排列的柱子上有著精緻的雕刻，讓人感到非常氣派。
站立在中間的，是身上穿著很講究的鎧甲騎士。手上拿著長劍，臉上戴著面具隱藏了起來。
就算看習慣了牛頭人和哥雷姆的身影，還是覺得它的個子很高。
但是，在身上的氣氛就不同了。


死靈騎士。


奧加王就是在這裡失去第一個夥伴。


「散開！」
包含瑪魯的肉搏戰共4人，散開著包圍死靈騎士。
不能拖長時間。沒有抵抗力的人類，只是在這裡就會被奪走生命力。那就是死靈騎士的能力。


死靈騎士的視線，看向體型最大的紀咕。
而那個視線有毒。紀咕按住胸口，單腳跪了下去。


另外，沒有拿劍的左手指向卡洛斯。
「嗚咕」
左手感覺到了異常，卡洛斯的盾變的沉重而失去平衡。


「嘿呀啊啊啊！」
莉雅用八雙揮下了刀攻擊，死靈騎士用長劍擋住。如果是體質不好的劍早就斷了吧、然而那把劍是魔劍。
轉動刀身來接住攻擊也很厲害。戰鬥技術非常高。
 在背後的瑪魯用箭射擊，雖然貫穿了鎧甲，但是沒有傷害的樣子。



被露露用魔法回復的紀咕揮下了戰槌，被用華麗的動作躲開了攻擊，衝到了腋下旁。

「喀…」

鮮血飛散。在那同時紀咕感覺生命力被奪走。



在那瞬間露出的空隙，莉雅沒有放過。

逆袈裟斬往上攻擊，死靈騎士用劍化解了，僅僅靠氣勢殺不了，但頭盔被打飛了。

(z:逆袈裟斬是由腳邊往上斜斬的技術，有興趣自己去查吧...)



在那裡的是，果然還是骸骨的頭部。

掉落地面的頭盔化成霧消失了，而頭部也恢復了。

瑪魯射出的箭，腐蝕掉落地面、鎧甲上的傷也再生了。



「歐內醬，不行！我的魔法沒辦法解除！」

在卡洛斯旁的薩基發出悲鳴。

為了治療紀咕露露趕了過去。薩基向露露施展了加速魔法。以死靈騎士的動向來看，露露何時會被殺都不知道。



瑪魯裝填之後射擊命中了，但沒有影響的樣子。

「莉雅醬、這傢伙不是神聖魔法就沒效果啊！」

瑪魯一副快哭的聲音叫著。

斥候、前衛、後衛，莉雅的隊伍看上去平衡不錯，但是、並不是十全十美。主要對付不死系的魔法，隊伍上並沒有神聖魔法的使用者。

在魔法相關方面進行研究的魯霍蘇，向神祈禱發出威力的神聖魔法，不是神殿管轄的人員就無法施展。而他的弟子莉雅和露露沒有經驗。

「大丈夫！傷害也不是沒有上升！」

薩基叫著。鑑定連對手的生命力也能看到。這種場合，應該說是負的生命力吧。



「那樣的話，就慢慢地削下去吧」

莉雅把刀收起，兩手拿出戰槌。以魔劍為對手，判斷刀刃難免會缺損。

復歸的紀咕開始攻擊。莉雅環繞著它攻擊、死靈騎士開始沒有避開的餘裕。

在那期間露露解除了卡洛斯左手的詛咒，三人持續的攻擊著。

死靈騎士使用特殊攻擊讓卡洛斯和紀咕無法動彈，露露馬上施展治癒。



薩基朝著開了洞的地方魔法攻擊。



意外的最後一擊竟然是被瑪魯放的箭刺中。



死霊騎士倒下，身體變成灰崩落。

在那之中存在著魔結晶，還有留下的武器和防具。



「沒有詛咒。防具和劍有自動修復機能、而劍斬向敵人有奪取體力的能力」

薩基鑑定著，莉雅充滿興趣的把劍拿在手上。

「刀的話…。鎧甲尺寸不合、劍的話卡洛斯要用嗎？」

「誒、可以嗎？」

長劍用單手就可以拿的重量，卡洛斯拿比較適合吧。這樣想著、但實際在揮時好像有點問題。

「在探索結束前，還是使用現在的劍吧。果然順手的東西比較好」

現在使用的劍也是新品，長度和平衡經過仔細的挑選。在戰鬥中有些微的不協調、都會造成致命吧。

莉雅只要是刀哪個長度都能用，但恐怕在實戰中也不能每把都拿來試吧。

「那麼薩基、収納吧。去下個階層再休息」



雖然打倒了死靈騎士，但在墓地休息還是有點介意。

露露治療著異常狀態，喝了魔力回復的魔法藥、一行人踏往了第8層。
