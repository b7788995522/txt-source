在神光國準備完成的我們，前往神光國郊外，正好在街道和障壁塔中間位置的場所。

這附近沒有什麼人，所以可以盡情發揮超人般的腳力。雖然速度驚人，但對影耶的身體來說只是小跑步而已。身為魔人擁有優秀身體能力的夏緹亞也無事地追著我跑。

「⋯⋯啊！小影耶的大腿有投擲小刀的綁帶！你這是想展現自己的美腿嗎！」
「沒、沒關係吧，又不會怎樣⋯⋯⋯畢竟是夏緹⋯⋯母親費盡心思做出來的身體⋯⋯」
「好可愛～～！」

⋯⋯只要我是影耶的樣子，夏緹亞沒事就會調戲我來玩。雖然現在自己也很享受這副樣子，但被這樣說果然還是有點害羞。

「不過，不就是因為這樣所以光彌才會喜歡上你的嗎？而且還用那種容易讓人誤會的態度對人」
「我倒是自認都是採取會讓他討厭的舉動⋯⋯」

難道，光彌和安古一樣是Ｍ嗎？⋯⋯我應該反過來對他溫柔點才對嗎？

我們就這樣聊東聊西，不知不覺地到達目的地。
目的地附近設置著幾個帳篷樣的東西，還有一些簡單的防御用柵欄。要比喻的話應該就是極為簡易的軍事據點吧。

這裡是神聖騎士用來攻略障壁塔的據點。他們似乎是以少數精銳潛入的，所以沒有太大規模的營地。
而那些神聖騎士則好像被叫做哈魯梅的不死族給殺掉，不過好像有幾名騎士為了調查攻略完障壁塔後還待在這個據點。

夏緹亞語調輕鬆地向其中一名騎士搭話。

「呀呼～勇者來了嗎？」
「夏、夏緹亞大人！抱歉，勇者還沒到來。」
「哼嗯。嘛我也來早了。謝謝。」

夏緹亞被騎士尊敬也是有理由的。
想說要把背景一切不明的夏緹亞加到勇者隊伍中實在有些太勉強的我，用金錢和恩情幾乎是半強迫地和神光國國王交涉，讓夏緹亞暫時成為了神聖騎士。

劇本是這樣。

被不死者英雄哈魯梅所召喚的魔人夏緹亞，對哈魯梅的暴行感到痛心。無法違反契約的她看準一瞬間的空隙逃了出來。接著她來到唯一認識的影耶身邊，請她覆蓋契約，獲得脫離邪惡的神聖之力的夏緹亞，用光輝的一擊，打敗了想要毀滅市街的哈魯梅。因為想要幫忙一起討伐魔王，所以請國王看在拯救神光國的份上行個方便。具體來說就是給夏緹亞拯救市街的神聖騎士稱號，並隱瞞她是魔人的事實。

⋯⋯嘛，雖然摻雜一堆謊言，而且之後也肯定會添麻煩，不過就請看在我們打倒想要毀滅國家的凶惡魔物的份上原諒我們吧。就算真發生什麼麻煩事，應該也不會演變成什麼大慘劇。大概。

稍微等了一會兒，金屬龍二世拉著馬車來到。

從馬車下來的光彌等人，看向我身邊沒見過的女騎士問道。

「影耶，這個人事？」
「她是──」
「初次見面，我是小影耶的媽媽♪」

⋯⋯就算是這情況也要貫徹母女的設定嗎。

不過至少說是妹妹之類的吧。你看光彌他們都一臉問號了。

「⋯⋯那個⋯⋯」
「啊啊，嗯。這個人只是自說自話而已不用在意。我也不是真的是她女兒」
「才不是呢，小影耶就是我的孩子」
「知道了知道了。這樣下去沒法討論所以母親你安靜一下」

我重新振作，一邊回想事前考慮好的設定一邊說出口。

「這個人的名字是夏緹亞，是我的舊知，也是這個國家的神聖騎士。雖然至今不是很有名，但前幾天攻略了障壁塔而出名了。」
「⋯⋯雖然在路上就有聽說過傳聞了，但沒想到障壁塔真的被攻略了」
「啊啊，這邊詳細的⋯⋯母親，說明下」

我交給夏緹亞說明。這是為了確定她的演技力還有是不是有好好記著設定。
不過我也不是特別擔心，就算真有萬一我也可以從一旁輔助。

「先行的騎士們雖然殲滅了塔中的魔物，但問題是出在頭領魔物身上。然後我在那個頭領魔物向著市街放出大魔法前打倒了他，但最上層張開著結界。」
「結界嗎？那我想我的神聖魔法應該可以無效掉那個結界。因為攻略完鐵帝國的障壁塔，我的魔力也上升了。」
「那個⋯⋯⋯嘛，算了。懶得說明，我就直接帶你們過去看看如何？」
「⋯⋯可以是可以」

雖然有點不安，不過設定好像有好好記得所以應該沒事吧。

坐上馬車的我們，在夏緹亞的帶路下前往障壁塔。

※

因為魔物都已經被殲滅了，所以我們無事地抵達最上層的前面。我們雖然也有設置陷阱，但時機還不對所以先暫時停止了。

包圍最上層的結界，是由令人不舒服的灰色魔力所構成。

「《褉之波動》！」

光彌的神聖魔法向周圍放出。雖然想拜託他不要連我都卷進去，不過專門防御這個的道具『邪龍之環』會無效掉神聖魔法所以沒問題。雖然發動一次就會壊掉必須再用改造魔法修理才行。

不管這個，最上層的結界儘管吃下光彌的神聖魔法，卻還是沒有一點損傷。

「什麼⋯⋯」
「守護隔絶褉石的這個結界，應該是用高等級的邪惡魔法展開的。除了單純用超強力的魔力直接衝擊，和等待時間經過外幾乎是不可能可以無效化的。」

夏緹亞一邊這樣說，一邊看著自己展開的結界。雖然背著臉所以其他三個人看不到，但夏緹亞現在表情實在是得意到了不行。

「然而雖然說需要時間經過，但應該也不會花上多久。就我來看應該就再四天八小時左右吧」
「唔喔⋯⋯竟然如此具體，真不愧是神聖騎士」
「嘛、嘛。畢竟我是天才」

被安古稱讚讓她口吃了一下。

「既然這樣，比起先去攻略其他國家的障壁塔，在這個國家等待結界解開應該會比較好。這樣幾乎不用冒険就能強化光彌。」
「恩，王女大人說的沒錯。然後在此我想向光彌提案一下⋯⋯要不要修行一下？」
「修、修行？」
「啊啊，只是空等也太浪費了。光彌好像還很缺乏近戰的實戰經驗，所以多做一點戰鬥訓練不也不錯嗎？雖然旅行間請小影耶教你也可以，但小影耶不擅長教人呢」

畢竟我只是全靠道具的劍士。隨便揮劍，劍就會自己砍過去，所以根本沒什麼可以教人的。

「原來如此⋯⋯⋯我明白了，就拜託你了，夏緹亞小姐！」
「恩，這邊也請多多指教。我可是相當嚴格，你可要做好覺悟喔？」

我在心中獨自詭笑。
修行當然不可能普通地修行。裝作修行實際上就是要找機會做掉光彌。

「那麼，就先從高空彈跳開始吧！」
「欸？」

※

在高空彈跳的途中砍斷繩子推下光彌。

「啊啊啊啊啊啊啊！褉、《褉之衝擊》！！」

然而，光彌卻放出魔力抵銷衝擊力，普通地著地。

「嘁⋯⋯」
「⋯⋯這真的是修行嗎，夏緹亞閣下？我怎麼感覺好像只是普通地想要殺掉⋯⋯」
「不不不，剛才的也是修行唷修行」
「沒錯安古，趕快往下一個去吧」

※

把全身被緊縛的光彌丟到魔物面前。

「唔、唔喔喔喔！」

然而，光彌卻用纏著魔力的手刀砍斷繩子逃了出來。

「⋯⋯很好，剛才光彌已經學會如何用手刀戰鬥了。恩，這也是修行唷修行」
「不，剛才怎麼看都⋯⋯」
「你在說什麼啊光彌，你以為母親會什麼都沒想就把你丟到魔物面前嗎？」
「對對對，這也和我想的一模一樣喔！好了繼續下一個吧！」

※

把光彌逼到沒有退路的地方，用巨大的鐵球撞向他。

「哈、哈⋯⋯！」

然而，光彌卻空手用手刀在地上挖出洞來，從那邊逃掉了。

「這、這次我真的覺得要死掉了⋯⋯！」
「嘁」
「你剛才咂嘴了對吧！？果然是想殺了我沒錯吧！？」

⋯⋯這樣下去實在有點不妙。再繼續被懷疑就很難殺掉光彌了。

「母親，差不多可以普通地修行了吧。那個，就是。你看，光彌的覺悟已經展現的夠充分了吧」
「⋯⋯也是呢。對不起光彌。就是，那個。要打倒魔王需要有拼上性命的覺悟。你看，為此我也準備了復活道具。」

這樣說完，夏緹亞拿出我事先交給她的復活藥。復活道具在這個世界是最高等級的貴重品。就算是大國有沒有一個都不好說。順便一提我有五個。

「復、復活道具！？安古，那是真貨嗎⋯⋯」
「啊啊，毫無疑問，是真正的復活藥。而且效果還相當高的優質品⋯⋯⋯這大概是傳說級的物品了吧」
「要體驗死亡就不能用不夠徹底的方式。而且如果是為了拯救世界的勇者大人，花上蘇生藥一瓶兩瓶也是值得的。」

事前考慮好的開脫理由，讓光彌等人安下一顆心。
嘛從一開始就打算讓光彌復活，所以也不是騙人的。

我裝作自然地接近夏緹亞，在他耳邊提議變更計劃。

「（差不多該變更計劃了吧？）」
「（OK。接下來要用哪個計劃）」
「（用Ｂ計劃。在街上獲取一定程度信賴後，就看準他大意的時候殺掉他。行嗎？）」
「（了解，就交給我吧。）⋯⋯那麼，天色也暗了，就回去街上吃個飯吧！光彌也告訴我你經歷過哪些戰鬥唷？」

夏緹亞這樣說著，踏起腳步回去街道。

總之，今天就先這樣吧。要是做太多被懷疑反而麻煩，今晚就普通地幫夏緹亞和光彌打好關係吧。

「不錯呢，這個國家的料理聽說也很美味。有名的記得是甜點沒錯？」
「嘿，是這樣嗎王女大人」
「是唷，影耶也喜歡吃甜食嗎？真意外」
「是、是這樣嗎？那個，所以有名的甜點是什麼東西呢？」
「巧克力喔。最近加入酒精的口味很有人氣。」
「嘿⋯⋯」

影耶的身體對酒精有點弱讓我有些不安⋯⋯⋯嘛，就加到巧克力裡的那點酒應該是不會醉吧。會是什麼味道呢真期待啊。


════════════════════

Ｑ.是FLAG嗎？
Ａ.是FLAG