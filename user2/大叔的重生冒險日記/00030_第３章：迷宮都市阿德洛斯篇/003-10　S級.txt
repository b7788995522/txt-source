在王都冒險者公會的公會長辦公室，當日立刻領取了閃耀的S級的冒險者卡。擁有彩虹色的光澤，雖然不是魔法金屬，但好像是用特別的素材做出來的。
「接下來該怎麼辦？」

「按照預定。開路邊攤。這樣一來，如果那些討債的人放棄，就不會對繪理一家出手了。如果出手就毀掉。確實是借了哪筆錢。趕快還債，為今後的生活立下目標。」

「嗯。不過你能不能定期拿出高級藥水呢？」
「啊。謝謝你的幫助，但是不是說不能拿太多出來嗎？」
我，睜大眼睛，充分戲劇性地說了。

「不，都走到這一步了，應該沒問題了吧。被分散關係的貴族們，讓你活著變成他們自己的利益。一旦到了關鍵時刻，估計支持我們的人也會很多。而且你在A級考試中表現得那麼華麗。還會伸出手來的傻瓜也不多見吧。」

不愧是吉爾馬斯，想了很多呢。那我就不客氣了。

「還有，我們希望接收龍，你想怎麼辦？」
「先問安東尼奧。他一直留著的樣子。我想自己留著。」

「你想用來幹什麼？」
「排成一排，對威脅傻瓜非常有效。」
稍微用得意的表情說一下。還剩下大掃除。

「算了，算了。」
「那我回去了。我打算暫時待在阿德洛斯。」
「知道了。」

和繪理她們商量的結果，一開始是賣甜甜圈和吉拿棒。

雖然覺得賣油炸品會怎麼樣呢，但是艾莉熱情地說一定會被接受的。既然這傢伙這麼說的話，就就試試看吧。

慎重起見，為了防備燒傷，為繪理準備了很多藥水。

路邊攤的設備利用空閒時間，順利地做完了。還做了吉拿棒形狀的口金。奶油甜甜圈也完成了。用這個世界的素材製作了糕點奶油。
（譯註：
【口金】，一種金屬配件，有一個孔，附在【擠壓袋】的尖端，用於奶油裝飾。
【糕點奶油】，カスタードクリーム，用蛋黃和糖混合均勻，加入麵粉，一點一點地加入溫熱的牛奶，加熱直至變稠。一般用香草調味。與生奶油不同，它在加熱時品質不會改變，並且經常使用，因為它價格便宜。但是，由於它含有大量水分，因此很難長時間保存。因為這個原因，經常添加酒精作為對策。）

艾莉是狂喜。這樣的話萬一我不在，奶油泡芙也快要能吃了。

為了慎重起見，最初的時候，安排別的C級隊伍作為護衛。如果發現奇怪的動作，請向我通報。

第二天在迷宮前的廣場，設置路邊攤開始買賣。因為組合著賦予減輕重量的魔石，因此繪理一人也輕鬆地拉著。

看起來好奇的人遠遠地看著。然後，豎起了【旗幟】。用小木屋的材料做成了混凝土製的旗幟台。

在便利店之類的地方用的東西。垂直的旗子，用這裡的文字寫著甜甜圈，吉拿棒之類的。

然後分發了試吃品。保羅和瑪麗也幫忙分發了。為了這一天也準備了服裝。你們真可愛。

繪理和麗莎在準備。萬一不夠的話，我可以從道具箱裡拿出庫存。因為可以複製，所以不會缺貨。但是總有一天，我不會幫忙的。

只是小蘇打有點麻煩啊。過去在地球，那是怎麼做的？我只是用帶來的複製，所以不太清楚。在網路上看做法，沒用道具箱，實際上要從頭開始做。
（譯註：重曹，小蘇打。）

發酵粉有碳酸氫鈉的話，想辦法用這個世界的材料也能做。總之現在我正在準備材料。發酵粉也是從地球的食物中分解出來的。

試吃品的評價非常好。買回去的人也很多。從這個世界的材料和人工費來計算，考慮到將來的事情，只賣2枚銅幣。還要交稅。

路邊攤的話，要付給商業公會的場地費是一天2枚大銅幣。有代官名的營業執照。

小攤子是零售業者，把握銷售額也很麻煩。雖然支付是一律的，但如果銷售額減少的話，材料費、燃料費等相加之下，就會出現虧損。

「喂！是誰讓你在這做生意的？」
一個令人害怕的聲音突然大喊起來。

「是代官大人。」
是真的，沒辦法。

「什麼呀。別開玩笑了。這附近的攤子都歸我管。快付保護費！一天五枚大銅幣。」
不要說這種話。
（譯註：ショバ代，保護費。）

我笑著接近那傢伙，用目不轉睛的速度伸出手。向空無一人的地方扔出去。然後，拿出龍的頭放到他眼前。

「嗚哇～～。」
那傢伙癱軟了。因為只會對弱者耍威風，所以才會這樣。

「那個龍，嘴巴有點兒笨，所以我【教訓了】。你也需要教訓一下嗎？」

順便說一下，在另一邊，也放下了滿身是血的可恨表情的奇美拉的頭。

「啊——。」

那傢伙慘叫著逃走了。

果然這個頭能用！太可惜了，不能賣。請還活躍吧。我想要新的。去北方的迷宮看看吧。

長廊的市民們哄堂大笑。（譯註：ギャラリー，長廊。）
「你真能幹，大哥。你是傳說中的屠龍者嗎？什麼是S等級？」
我微笑著，展示閃閃發光的S級卡。

「別為難了，阿爾馮斯先生。別搶走我們的工作。」
一旁警備的冒險者笑著說。

「什麼，數量就是力量。他們也放棄了吧。拜託你了。」
因為成功聚集客人，從第一天開始，繪理的路邊攤就熱烈銷售了。

然後連日來熱鬧非凡。因為有其他的冒險者在警備，所以艾莉也加入了路邊攤。這傢伙出乎意料地靈巧，那樣的工作也全能勝任。

因為她既漂亮又可愛，所以在男性顧客中評價也不錯。要是妳沒那麼貪吃的話。

那些傢伙在那之後，就沒再看到了。大概在看情況吧。果然，不崩潰不行啊。

在異世界的第63天。做了10天的買賣，繪理她們也存了很多錢。是時候了。

我訪問了阿德洛斯的代官邸。
「你好。我是S級冒險家阿爾馮斯。不是貴族，不過，是伯爵同等的身分。我想見代官。」
我只在這種時候使用，用拿手的貓一般聲音拜託了。

我與代官會面，請求減輕繪理的家的利息的計算。一般不會這麼做，不過，用S等級&單獨屠龍者W的威光，救助了王子的英雄的名字價值，請求被接受了。

據代官說，那從以前開始，就是個大問題。

「因為他們做著過分貪婪的買賣，我只能以苦悶的表情看著。因為惡黨與貴族混在一起，所以無法插手。這次的事情也是關於同在一條船的問題。」

與代官，和武裝了的家臣們一起進入討債的地方，逮捕了全體人員。如果抵抗的話就輪到我出場了。用不著拔劍。

不需要魔法。被迷宮的魔物們很活躍。是階層主首級的遊行。這裡，你看，不能減少崇拜啊。

為了強化身體，揮舞著沾滿鮮血的巨大高等魔物的頭顱的我，被這附近的惡徒們作為恐怖的代名詞流傳下來。據說狩獵首級阿爾馮斯的傳說，流傳了很長時間。

我太興奮了，鬆了一口氣。好像收拾了累積一周份洗過的衣服似的，很舒暢。總之，就是從煩人的吉爾馬斯那裡得到OK的財產。扣押證件等證據。和貴族的備忘錄什麼的。

我的MAP檢索，它們絕對逃不掉。一次又一次地變換搜尋關鍵字，一個勁地檢索。我盡力了。揭露了一切。

他們積蓄的錢也全部沒收，在利息再計算上從過多部分扣除本金，重新分配。

不過不能全部都還回去。「錢還回來的話，不開心嗎？」這樣的事請忍耐。因為太貪圖暴利，所有人的本錢返還被認可了。

代官，是像便秘治療了一樣的流暢的表情。因為壞蛋貴族直接暗中活動，代官好像被忽略了。代官好像完全沒有分到。我還以為他是缺德貴族。

只是打算讓你算一下而已。代官說。
「要做的話不徹底做的話，自己會更危險。因為我不是貴族也不是什麼東西。只能依靠妳！」

繪理她們的份先算了。退了三十塊銀幣。繪理一家看起來也很開心。

作為證據的文件，為了不讓它們在途中消失，通過羅巴的爺爺直接送到國王陛下面前。阿爾伯特洛斯貴族們的所作所為被暴露出來，王國再也不能不處分了。

3個子爵家、10個男爵家永遠地從這個地方消失了。阿爾伯特洛斯王國的貴族社會，經歷了前所未有的強烈震動。

伯爵以上的貴族，大致上是歡迎氣氛。那些做著與身份不相稱的人們，貶低了王國貴族的名譽，當然要處理。

子爵以下的貴族都顫抖著，以為明天就會輪到自己。當然，下級貴族中也有尊重清貧的人，不是全部。

在阿德洛斯做壞事，此次偶然逃過肅清的人，都戰戰兢兢。他們悄悄撤退，為了不留下證據。

我的惡名在這個國家的貴族社會裡，如雷貫耳。

「貴族殺手的屠龍者。」

哎呀，滿足了，滿足了。（譯註：本願，滿足。）

但是，這並沒有拯救到阿德洛斯的窮人們。問題並沒有消失。也許有人會因為沒有應急的手段，而感到為難。

但是像繪理這家一樣，在借錢之前，如果變得更窮的話，就更難開口了。

想了很多，我向代官提出建議，如果能創造什麼工作機會的話。
雖然具體的內容還沒有公佈，但行政領導人積極向上並不是壞事。

並且，因為現在這個世界有「網際網路」的力量。