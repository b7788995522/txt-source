「但是，那只是一般的情況，其它殺死吸血鬼的辦法，也不是沒有」

伊薩古繼續往下說
我們都認真傾聽

「例如，聖氣。那種東西對我們來說非常棘手，僅僅接觸到就會被燙傷，注入武器中的話，就能造成沉重的傷害⋯⋯即便如此，也不至於一擊斃命，但之後的再生速度難免減緩不少，次數也會被一口氣縮減吧」

伊薩古之前確實對我製造的迷你聖樹表露出厭惡之情
雖然非常微弱，但它確實有放出聖氣
現在還留在庭院的一角，伊薩古應該完全不想靠近那邊吧
是真的很不擅長那個呢
雖然拔掉就好了，但這也是他對菈烏菈忠誠的一種表現吧

話說回來，在吸血鬼之間的戰鬥中，減少對方的恢復速度與次數，可是具有很大意義的
如果做不到這種事情的話，戰鬥就會長時間拖延下去
仔細想想，在新月迷宮裡的時候，妮薇或許就是這樣做的
她比我更擅長運用聖氣，我也沒法判斷當時她有沒有使用

如果是聖炎的話，就能清楚看到，但戰鬥中一般不會使出那招吧
雖然我學會了隱藏聖氣，但還是沒法與經驗豐富的妮薇同日而語
就是這麼回事

回想起來，當時遇到的吸血鬼們之所以感到驚訝，也許是因為知道再生是有限度的，但消耗得比想像中還要快的緣故吧
原因當然是妮薇的聖氣了
值得一提的是，故意不透露這件事，打擊敵人的自信心，妮薇的性格還真是夠惡劣的

嘛，也沒什麼不好的吧
那傢伙雖然性格有些惡劣，但也事到如今了
而且基本上，她也只對吸血鬼才如此冷酷
其它時候還是很正常的

「聖氣之外的手段沒有了麼？」

羅蕾露這樣問到，伊薩古點了下頭

「有的⋯⋯還有一種辦法」

這樣說著，他突然從空無一物的空間中取出一柄劍來
看起來只是輕輕晃了一下胳膊⋯⋯

到底從哪裡取出來的？
戲法麼
伊薩古是所謂變戲法的①麼？

⋯⋯應該不是吧

順便一提，所謂變戲法的，是指不使用魔術的手段，產生類似於魔術現象的人
有時候，使用魔術也辦不到的效果，卻能夠通過詭計來實現
雖然聽上去沒什麼大不了的，但作為娛樂節目而言很有趣，所以也時常能看到

哎呀，現在是在說伊薩古剛剛拿出的劍

「那是⋯⋯你和施米尼戰鬥時用的劍嗎？感覺當時應該更大一些才對」

現在伊薩古拿在手中的，是一柄細劍
所以我才會這樣想。但隨著伊薩古用力握住劍柄，以細長的劍身為起點，劍刃的部分變得越來越大。最後，變成了當時看到的大劍

血紅色劍刃的大劍

⋯⋯好帥啊

但是，那武器是什麼東西？
是某種魔法劍嗎？
在意的不僅僅是我，羅蕾露也問到

「那個武器是⋯⋯魔法劍麼？我姑且也見過注入魔力後可以擴展劍身的機關⋯⋯」

姑且見過麼
嘛，魔法劍是由鐵匠們一邊反覆嘗試，一邊製造出來的東西
不論強弱，能想像到的機關，基本都被嘗試過
從這個意義上來講，也許並不稀奇，只是，那柄劍剛才確實是突然出現在伊薩古手邊的
有這種魔法劍麼？
不過，就算不是魔法劍的力量，部分轉移魔術也能辦到相同的事情⋯⋯嗯，還是難以判斷

是猜到我們的心思了麼，伊薩古開始解釋

「這是一種被稱為『鮮血武裝』的特殊武器。是吸血鬼的鍛冶師，以使用者的鮮血為材料，用特殊的方法製造出來的，可以被使用者收納在體內的魔劍。使用這個來戰鬥的話，即使對手是吸血鬼，也能像人類間的戰鬥一樣造成傷害」
「鮮血武裝」

還存在著這種武器麼
不，說了是魔劍，那麼也可以算作魔法劍的一種吧

魔劍和魔法劍的意思稍有不同，魔法劍是泛指所有含有魔力的武器，而魔劍一般則是指其中格外強大的東西

如果問羅蕾露的話，她應該會說這種說法並不準確，並且進行嚴密的定義吧⋯⋯話雖如此，魔劍這種東西，普通的冒険者基本是與之無緣的。想要入手的話，可是會飛走幾十、上百枚白金幣的

就算是我也買不起
我現在持有的劍，應該算在魔法劍的範疇內
效果只是能夠注入氣、魔力與聖氣而已⋯⋯用這種說法的話，庫洛普說不定會生氣啊
這無疑是一把好劍，所以被發火也是理所當然的
再說，只要有素材和錢的話，庫洛普說不定也能製造出被稱為魔劍的逸品來吧

「吸血鬼鍛冶師，擁有製造魔劍的技術麼⋯⋯」

羅蕾露用非常驚訝的神情自言自語道
一般來說，魔劍之類的東西，都是在迷宮深處發現，從古代流傳下來，或是在拍賣會上出現的。沒有除此之外的入手途徑
而想要新製作出來，則是難上加難，正因為被認為不可能製作，才會稱之為魔劍②

所以，我能理解羅蕾露的心情
但伊薩古說到

「在我所知的範圍內，能夠製造出這種東西的吸血鬼鍛冶師只有一人，而且，他的技術也無人能夠繼承。所以，應該稱不上是吸血鬼掌握的技術。只有那個人別具一格而已」


════════════════════

Alice注：

①戲法，原文「手品」，變戲法的，原文「手品師」

一般譯法是「魔術」，「魔術師」

但奇幻作品裡的魔術師，一般是驅使魔力，引發不可思議現象的人啦，只好退而求其次

②這兩句問題很大啊
第一句把魔劍的產生途徑（迷宮）和流通途徑（流傳、買賣）混為一談，姑且不論
第二句怎麼就說「不可能とされているもの」被認為不可能（製造出魔劍來）

明明之前還說庫洛普「魔劍と呼べるものを作ることも出來るのかもしれないしな」說不定能製造出堪稱魔劍的東西
還是說，其實庫洛普是世界上頂尖的工匠，或者研究古代技術的第一人之類的麼