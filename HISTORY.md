# HISTORY

## 2019-08-21

### Epub

- [葉隠桜は嘆かない](ts/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84) - ts
  <br/>( v: 3 , c: 63, add: 0 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 5 , c: 66, add: 2 )
- [転生ごときで逃げられるとでも、兄さん？](yandere_out/%E8%BB%A2%E7%94%9F%E3%81%94%E3%81%A8%E3%81%8D%E3%81%A7%E9%80%83%E3%81%92%E3%82%89%E3%82%8C%E3%82%8B%E3%81%A8%E3%81%A7%E3%82%82%E3%80%81%E5%85%84%E3%81%95%E3%82%93%EF%BC%9F) - yandere_out
  <br/>( v: 5 , c: 153, add: 32 )

### Segment

- [葉隠桜は嘆かない](ts/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84) - ts
  <br/>( s: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )
- [転生ごときで逃げられるとでも、兄さん？](yandere/%E8%BB%A2%E7%94%9F%E3%81%94%E3%81%A8%E3%81%8D%E3%81%A7%E9%80%83%E3%81%92%E3%82%89%E3%82%8C%E3%82%8B%E3%81%A8%E3%81%A7%E3%82%82%E3%80%81%E5%85%84%E3%81%95%E3%82%93%EF%BC%9F) - yandere
  <br/>( s: 2 )

## 2019-08-20

### Epub

- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( v: 2 , c: 39, add: 2 )
- [自覚なし聖女の異世界モフモフ道中](girl/%E8%87%AA%E8%A6%9A%E3%81%AA%E3%81%97%E8%81%96%E5%A5%B3%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E3%83%A2%E3%83%95%E3%83%A2%E3%83%95%E9%81%93%E4%B8%AD) - girl
  <br/>( v: 1 , c: 24, add: 13 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( v: 6 , c: 49, add: 0 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 5 , c: 64, add: 2 )

### Segment

- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( s: 3 )
- [自覚なし聖女の異世界モフモフ道中](girl/%E8%87%AA%E8%A6%9A%E3%81%AA%E3%81%97%E8%81%96%E5%A5%B3%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E3%83%A2%E3%83%95%E3%83%A2%E3%83%95%E9%81%93%E4%B8%AD) - girl
  <br/>( s: 2 )
- [魔王様、リトライ！](user/%E9%AD%94%E7%8E%8B%E6%A7%98%E3%80%81%E3%83%AA%E3%83%88%E3%83%A9%E3%82%A4%EF%BC%81) - user
  <br/>( s: 8 )

## 2019-08-19

### Epub

- [世界最高の暗殺者、異世界貴族に転生する](syosetu/%E4%B8%96%E7%95%8C%E6%9C%80%E9%AB%98%E3%81%AE%E6%9A%97%E6%AE%BA%E8%80%85%E3%80%81%E7%95%B0%E4%B8%96%E7%95%8C%E8%B2%B4%E6%97%8F%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%99%E3%82%8B) - syosetu
  <br/>( v: 5 , c: 119, add: 87 )
- [把一億年按鈕按個不停](syosetu/%E6%8A%8A%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E6%8C%89%E5%80%8B%E4%B8%8D%E5%81%9C) - syosetu
  <br/>( v: 1 , c: 20, add: 20 )
- [異世界転性　～竜の血脈～](ts/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BB%A2%E6%80%A7%E3%80%80%EF%BD%9E%E7%AB%9C%E3%81%AE%E8%A1%80%E8%84%88%EF%BD%9E) - ts
  <br/>( v: 9 , c: 122, add: 122 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 5 , c: 62, add: 2 )

### Segment

- [把一億年按鈕按個不停](syosetu/%E6%8A%8A%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E6%8C%89%E5%80%8B%E4%B8%8D%E5%81%9C) - syosetu
  <br/>( s: 1 )
- [異世界転性　～竜の血脈～](ts/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BB%A2%E6%80%A7%E3%80%80%EF%BD%9E%E7%AB%9C%E3%81%AE%E8%A1%80%E8%84%88%EF%BD%9E) - ts
  <br/>( s: 72 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-08-18

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 5 , c: 60, add: 2 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 2 )

## 2019-08-17

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 58, add: 4 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 4 , c: 40, add: 2 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 4 )

## 2019-08-16

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 54, add: 2 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 4 , c: 38, add: 3 )
- [再見龍生你好人生](wenku8_out/%E5%86%8D%E8%A6%8B%E9%BE%8D%E7%94%9F%E4%BD%A0%E5%A5%BD%E4%BA%BA%E7%94%9F) - wenku8_out
  <br/>( v: 8 , c: 54, add: 54 )

## 2019-08-15

### Epub

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( v: 1 , c: 13, add: 13 )

## 2019-08-14

### Epub

- [世界最強の後衛　～迷宮国の新人探索者～](syosetu/%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7%E3%81%AE%E5%BE%8C%E8%A1%9B%E3%80%80%EF%BD%9E%E8%BF%B7%E5%AE%AE%E5%9B%BD%E3%81%AE%E6%96%B0%E4%BA%BA%E6%8E%A2%E7%B4%A2%E8%80%85%EF%BD%9E) - syosetu
  <br/>( v: 4 , c: 20, add: 18 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 52, add: 2 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 2 )

## 2019-08-13

### Epub

- [不是真正同伴的我被逐出了勇者隊伍，因此決定在邊境過上慢生活](dmzj_out/%E4%B8%8D%E6%98%AF%E7%9C%9F%E6%AD%A3%E5%90%8C%E4%BC%B4%E7%9A%84%E6%88%91%E8%A2%AB%E9%80%90%E5%87%BA%E4%BA%86%E5%8B%87%E8%80%85%E9%9A%8A%E4%BC%8D%EF%BC%8C%E5%9B%A0%E6%AD%A4%E6%B1%BA%E5%AE%9A%E5%9C%A8%E9%82%8A%E5%A2%83%E9%81%8E%E4%B8%8A%E6%85%A2%E7%94%9F%E6%B4%BB) - dmzj_out
  <br/>( v: 4 , c: 38, add: 17 )
- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( v: 1 , c: 203, add: 0 )
- [異世界薬局](syosetu/%E7%95%B0%E4%B8%96%E7%95%8C%E8%96%AC%E5%B1%80) - syosetu
  <br/>( v: 5 , c: 82, add: 0 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 50, add: 2 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 3 , c: 35, add: 0 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 0 )

### Segment

- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( s: 1 )
- [異世界薬局](syosetu/%E7%95%B0%E4%B8%96%E7%95%8C%E8%96%AC%E5%B1%80) - syosetu
  <br/>( s: 21 )

## 2019-08-12

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 48, add: 2 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 0 )

## 2019-08-11

### Epub

- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( v: 1 , c: 203, add: 203 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 4 , c: 46, add: 5 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 3 , c: 35, add: 2 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 0 )

### Segment

- [八男？別鬧了！](mirronight/%E5%85%AB%E7%94%B7%EF%BC%9F%E5%88%A5%E9%AC%A7%E4%BA%86%EF%BC%81) - mirronight
  <br/>( s: 146 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 2 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 1 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8
  <br/>( s: 23 )

## 2019-08-10

### Epub

- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( v: 2 , c: 37, add: 6 )
- [給鍬形蟲一記手刀結果穿越時空了](girl/%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86) - girl
  <br/>( v: 1 , c: 5, add: 5 )
- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 3 , c: 67, add: 2 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( v: 6 , c: 49, add: 3 )
- [中了40億圓樂透的我要搬到異世界去住了](wenku8_out/%E4%B8%AD%E4%BA%8640%E5%84%84%E5%9C%93%E6%A8%82%E9%80%8F%E7%9A%84%E6%88%91%E8%A6%81%E6%90%AC%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E5%8E%BB%E4%BD%8F%E4%BA%86) - wenku8_out
  <br/>( v: 10 , c: 236, add: 8 )

### Segment

- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( s: 4 )
- [給鍬形蟲一記手刀結果穿越時空了](girl/%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86) - girl
  <br/>( s: 4 )
- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 4 )
- [絶対に元の世界には帰らない！](ts/%E7%B5%B6%E5%AF%BE%E3%81%AB%E5%85%83%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%AB%E3%81%AF%E5%B8%B0%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81) - ts
  <br/>( s: 3 )



