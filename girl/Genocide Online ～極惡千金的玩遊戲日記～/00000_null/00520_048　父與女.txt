作者的話：
這是謝禮的連續投稿其４，這就是最後的了，真的是非常感謝各位。

很多人期望的現實中的麗奈


════════════════════

「大小姐，老爺請您過去一趟」
「⋯⋯⋯⋯我知道了」

一從學校回來就被山本先生叫住了。⋯⋯那個男人自己叫我過去？難道今天連雷陣雨也會下起來嗎⋯⋯⋯⋯⋯


▼▼▼▼▼▼▼

「來了嗎⋯⋯」

⋯⋯⋯⋯雖然我連片刻都不想看到這個男人的樣子呢。

「我不想看見你的樣子但有話要跟你說」

如果你也在思考相同的事就請不要故意叫我過來。

「⋯⋯說話是？」
「⋯⋯⋯⋯打算從分家的一條子爵家收養養子」
「能聽一下理由嗎？」

稍微跟不上對話呢，收養養子這種事不是不應該做的嗎？

「分家有跟你不同的優秀男孩在，我會將他當作繼承人」
「⋯⋯⋯⋯這個時候我不會說關於我的事的，但不是有弟弟在嗎？」

明明溺愛到那種程度了，甚至還向他露出過連我和臨終的母親都未曾見過的笑容⋯⋯⋯⋯⋯

「⋯⋯作為父親和作為一條公爵是不同的，那傢伙的人性沒有問題，但是他太過平凡了」
「是這樣嗎⋯⋯」

嘛，反正沒有跟弟弟和妹妹見面的打算，無論發生甚麼事我都不會在乎呢。

「有可能成為養子的五人，在人性上都比你更好，而且也很優秀⋯⋯⋯⋯跟你不同」
「⋯⋯」

就算我至今為止保持著模擬試全國第一位的成績，這個男人也不會賞識我，但現在他卻毫不動搖地說出這句話，就是說那些可能成為養子的人至少有與我同等程度的成績吧。

「這次他們會來這間宅邸拜訪，能請你不要離開別邸嗎？」
「不會離開的」
「哼，那時有關的人也會一起來的，我不想讓他們以為我有飼養一隻怪物」

⋯⋯⋯⋯⋯⋯⋯⋯我厭惡的，就是這個眼神呢⋯⋯⋯⋯我感到不快了，這種感覺就像是靈魂的底部黏住了污垢，巴不得想強硬地清除掉⋯⋯⋯⋯⋯⋯⋯

「⋯⋯⋯⋯怪物不要用這種眼神看著我，令人不快啊」
「⋯⋯⋯⋯⋯⋯⋯⋯非常抱歉」
「居然生出了這種怪物⋯⋯⋯⋯」

⋯⋯⋯⋯⋯⋯在母親臨終的時候明明和那個女人很相愛的？難道終於良心發現了嗎？

「⋯⋯妳說了什麼？」
「沒、沒有，什麼都⋯⋯沒有哦。」

啊啊，不可以的⋯⋯這是與母親最後的約定。無論是這個男人、那個女人，更不用說弟弟和妹妹，都不可以向他們出手的。

「那麼就立即從我的視線裡消失，盡可能不要打擾到會成為你義兄的人和我們啊？」
「⋯⋯⋯⋯⋯⋯⋯⋯失禮了」

壓抑著溢出的殺意鞠躬後，離開了房間。

「呼⋯⋯」
「大小姐⋯⋯⋯⋯」
「⋯⋯有甚麼事呢？山本先生」

離開房間後就看見山本先生正在等待著，還有其他甚麼事嗎？

「請不要太過介懷，這樣對身體有害的」
「謝謝，我很好」

啊啦，被稍微擔心了呢⋯⋯這樣可不行，似乎現在無法維持住面無表情呢。

「另外夫人和少爺他們差不多想見您一面了⋯⋯⋯⋯」
「請拒絕掉」
「⋯⋯⋯⋯我明白了，那麼我退下了」

說後山本先生離開了。因為現在見面的話可能就無法遵守與母親的約定了呢⋯⋯⋯⋯再說從一開始我就根本不想見面。

「⋯⋯⋯⋯尤其是從母親那裡搶走父親的那個女人⋯⋯⋯⋯⋯⋯⋯⋯」

避人眼目地回到別邸的我的房間後，我撲倒在床上。

「母親大人⋯⋯」

我把臉埋在只有一點點、總覺得實在只有一點點的母親香氣的枕頭裡，然後深呼吸。

「⋯⋯⋯⋯跟那個男人會面擾亂了我的感情，很疲倦呢」

將這股怨恨發泄在遊戲裡吧，恰好將要舉辦官方活動了呢⋯⋯⋯⋯⋯


▼▼▼▼▼▼▼

作者的話：
因為麗奈爸爸的緣故，玩家會⋯⋯⋯⋯？！

祝願你們在死後也能幸福⋯⋯（放棄）

在此之後會跟往常一樣在０點更新，謝謝。